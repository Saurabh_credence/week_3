const dm = require("./interface")
const od = require("./ordermandate")

module.exports = emitter => {
    emitter.on("security_create", async (msg) => {
        dm.security_create(msg)
    });
    emitter.on("companies_create", async (msg) => {
        dm.companies_create(msg)
    });
    emitter.on("exchange_rates_create", async (msg) => {
        dm.exchange_rates_create(msg)
    });
    emitter.on("ordermandate_create", async (msg) => {
        od.ordermandate_create(msg)
    });
}