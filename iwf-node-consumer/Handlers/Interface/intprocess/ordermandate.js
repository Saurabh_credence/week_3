module.exports = {
    "ordermandate_create": async function (obj) {
        try {
            let seq, finQry, insresult,result
            let iQry = []
            let updateOraDB = await funclib['updateOraDB']
            for (let k in obj.data.preorder_id) {
                seq = `SELECT iwf_preorder_id_seq.Nextval preorder_id_seq From Dual`
                result = await updateOraDB(`iwf`, seq)
                logger.info("Seq created : " + JSON.stringify(result.data.rows[0]))
                iQry[iQry.length] = `INSERT INTO iwf_eq_preorder SELECT '${result.data.rows[0]}' preorder_id`
                iQry[iQry.length] = `,a.scrip, a.ordertype, a.orderdate, a.quantity, a.rate, a.max_rate`
                iQry[iQry.length] = `,a.userid, a.ordervaliddate, a.ir_id, a.amount, a.squareoff, a.justification, a.remarks, a.in_portcode, a.out_portcode, a.bestprice`
                iQry[iQry.length] = `, a.mandateid, a.order_refid, a.isproduct, a.yield, a.asset_class_code, a.status, a.trade_instruction, a.invtype, a.is_amount_based_order`
                iQry[iQry.length] = `, a.premandate_id, a.price_from, a.price_to, a.yield_from, a.yield_to, a.sfin, a.pcy_mtm_value, a.pcy_order_amount, a.conversion_rate, a.soft_limit_breached, a.foliono, a.fd_product, a.fd_deposit_type, a.fd_daycount, a.fd_currency, a.fd_tenor, a.fd_is_frn, a.fd_roi, a.fd_interest_freq, a.fd_tax_status`
                iQry[iQry.length] = `, a.bucket_id, a.tif, a.brokercode, a.pool_order_no, a.mat_fd_product, a.mat_type, a.mat_addtnl_amt, a.application_no, a.hedge_desg, a.contractcode, a.lotsize, a.contracttype, a.noofcontracts, a.exchange ,'','','' FROM eq_preorder a WHERE preorder_id= '${obj.data.preorder_id[k]}';`
                iQry[iQry.length] = `INSERT INTO iwf_audit_eq_preorder SELECT b.*,'create' sys_auditreason,'' username,sysdate operationtime,'create' `
                iQry[iQry.length] = `operation,1 sys_audit_id FROM iwf_eq_preorder b WHERE preorder_id ='${result.data.rows[0]}';`
                iQry[iQry.length] = `INSERT INTO iwf_fm_portfoliowise_mandates (mandateid, preorderid, orderdate, portcode, security, quantity, rate, amount, percentage, deal_no, poolbatch, ordertype, userid, ordervaliddate, justification, remarks, trade_instruction, is_amount_based_order, bestprice, pcy_mtm_value, pcy_order_amount, conversion_rate, modelport) `
                iQry[iQry.length] = `SELECT (SELECT Max(MANDATEID)+1  MANDATEID FROM  iwf_fm_portfoliowise_mandates) mandateid, preorderid, orderdate, portcode, security, quantity, rate, amount, percentage, deal_no, poolbatch, ordertype, userid, ordervaliddate, justification, remarks, trade_instruction, is_amount_based_order, bestprice, pcy_mtm_value, pcy_order_amount, conversion_rate, modelport from fm_portfoliowise_mandates where preorderid='${obj.data.preorder_id[k]}';`
            }
            finQry = `BEGIN ` + iQry.join(" ") + ` END;`
            logger.info("IQRY : " + finQry)
            insresult = await updateOraDB(`iwf`, finQry)
            if (insresult.status) {
                logger.info("Order mandate created successfully.");
                return '{"status":"success","msg":"Order mandate created successfully.","error":""}';
            }
        }
        catch (error) {
            logger.error(error)
            return '{"status":"unsuccess","msg":"","error":"Error Occurred ' + error + '"}';
        }
    }
}